package Biblioteka;

    public interface FileManager {
        Library importData();
        void exportData(Library library);
    }
