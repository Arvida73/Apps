//package Discout;
//
//public class Discout {
//    Podzielmy kod na osobne klasy, aby poprawić jego uniwersalność.
//
//    Client - reprezentuje klienta,
//    DiscountService - klasa, która będzie zawierała logikę do naliczania rabatów,
//    DiscountApp - główna klasa, w której utworzymy klienta i naliczymy rabat, tak jak przed chwilą.
//
//    Klasa Client jest zgodna z konwencją JavaBeans i przechowuje informacje o imieniu, nazwisku oraz to,
//    czy klient jest klientem premium. Konstruktory, gettery oraz settery generujemy z pomocą środowiska, korzystając ze skrótu Alt + Insert lub ⌘ + N na MacOS.
//
//    Klasa DiscountService wygląda na pierwszy rzut oka na trochę skomplikowaną, jednak jeśli zaczniemy czytać kod od góry do dołu to okaże się, że dzięki odpowiedniemu nazewnictwu metod wszystko jest bardzo przejrzyste. Zwróć uwagę, że tylko metoda calculateDiscountPrice() jest publiczna, a to oznacza, że chcemy, żeby w innych klasach wywoływano tylko i wyłącznie tę metodę. Po co więc stworzyliśmy dodatkowe metody prywatne? Przede wszystkim dla czytelności. Zauważ, że dzięki odpowiedniemu nazewnictwu metod jesteśmy w stanie zrozumieć kod po prostu go czytając:
//
//    double calculateDiscountPrice(Client client, double price) - zwróć cenę po rabacie dla wskazanego klienta i podanej ceny wyjściowej,
//            if(client.isPremium()) - jeżeli klient jest premium,
//return calculatePremiumDiscount(price); - to nalicz mu zniżkę premium
//    W tym momencie przechodzimy do metody obliczającej rabat premium.
//
//    double calculatePremiumDiscount(double price) - oblicz rabat premium dla wskazanej ceny,
//if(price > 1000) - jeżeli cena jest powyżej 1000zł,
//            return applyDiscount(price, 0.15); - to zaaplikuj zniżkę w wysokości 15%
//    itd.
//    Mam nadzieję, że rozumiesz ideę. Dzięki temu, że metody są prywatne, to mamy gwarancję, że nikt poza tą klasą z nich nie korzysta. Jeśli w pewnym momencie zdecydujemy się np. usunąć którąś z tych metod, zmienić ich nazwę, czy poprawić kod, to mamy gwarancję, że nie zepsujemy tym samym innych części aplikacji, gdzie ktoś mógł się do tych metod odwoływać. Jedyne na co musimy uważać to zmiany dokonywane w metodzie publicznej (np. przez dodanie do niej dodatkowego parametru, albo zmianę typu zwracanego).
//
//    Spójrzmy jeszcze na klasę testową, gdzie utworzymy dwóch klientów i naliczymy im różne rabaty.
//
//            DiscountApp.java
//
//
//}
