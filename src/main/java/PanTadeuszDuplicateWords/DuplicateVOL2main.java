package PanTadeuszDuplicateWords;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

    public class DuplicateVOL2main {

        public static void main(String[] args) throws Exception {
            String path = "/Users/damiankopacz/IdeaProjects/App/src/main/java/PanTadeuszDuplicateWords/PanTade.txt";
            List<String> lines = Files.readAllLines(Paths.get(path), Charset.forName("UTF-8"));

            DuplicateVOL2 wordCounter = new DuplicateVOL2();
            wordCounter.processLines(lines);
            wordCounter.getMostPopular(10).stream().forEach(System.out::println);
            System.out.println("------ONLY ONE----------------");
            wordCounter.getWithNumberOfOccurances(1).stream().forEach(System.out::println);
        }

    }
